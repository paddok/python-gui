import tkinter
from tkinter import ttk

win = tkinter.Tk()
win.title("Python GUI")

tabControl = ttk.Notebook(win)
tab1 = ttk.Frame(tabControl)
tabControl.add(tab1, text='Tab 1')

lblF_mighty = ttk.LabelFrame(tab1, text='Mighty python')
lblF_mighty.grid(column=0, row=0, padx=8,pady=4)

lbl_a = ttk.Label(lblF_mighty, text="Enter a name: ")
lbl_a.grid(column=0, row=0, sticky='W')

tab2 = ttk.Frame(tabControl)
tabControl.add(tab2, text='Tab 2')

lblF_mighty2 = ttk.LabelFrame(tab2, text="The Snake")
lblF_mighty2.grid(column=0, row=0, padx=8, pady=4)

chVarDis = tkinter.IntVar()
btn_chk1 = tkinter.Checkbutton(lblF_mighty2, text="Disabled", variable=chVarDis, state="disabled")
btn_chk1.grid(column=0, row=0)

tabControl.pack(expand=1, fill='both')


#===========================================
# Start GUI
#===========================================
win.mainloop()
